<?php

namespace App\Actions\Api;

use App\CustomTrait\Formatter;
use App\Http\Requests\Api\StoreUserRequest;
use App\Http\Requests\Api\UpdateUserRequest;
use App\Models\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\Auth;

class UserAction
{
    use DispatchesJobs, Formatter;


    public function getAllUser()
    {
        $user = User::paginate(10)->toArray();
        $data = $user['data'];
        $meta = $this->metaFormatter(
            $user['current_page'],
            $user['from'],
            $user['last_page'],
            $user['links'],
            $user['path'],
            $user['per_page'],
            $user['to'],
            $user['total']
        );

        $result = $this->paginationFormatter($meta, $data);
        return $result;
        return $user;
    }


    public function createUser(StoreUserRequest $request)
    {
        $user = User::create([
            "name" => $request->name,
            "email" => $request->email,
            "password" => bcrypt($request->password),
            "username" => $request->username,
            "phone" => $request->phone,
            "role_id" => $request->role_id,
        ]);

        return $user;
    }


    public function getUserById(User $user)
    {
        return $user;
    }


    public function updateUserById(User $user, UpdateUserRequest $request)
    {
        $input = $request->only([
            'name',
            'username',
            'phone',
            'role_id',
        ]);

        $user->update($input);
        return $user;
    }


    public function deleteUserById(User $user)
    {
        $user->delete();
        return $user;
    }
}
