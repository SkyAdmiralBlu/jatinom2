<?php

namespace App\Actions\Api;

use App\CustomTrait\EggProductionChart;
use App\Models\EggProduction;
use Carbon\Carbon;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;

class EggProductionAnalyticAction
{
    use DispatchesJobs, EggProductionChart;

    public function getEggProductionAnalytic(Request $request)
    {
        // Ambil value query
        $periodFilter = $request->query('period');
        $rangeFilter = $request->query('range');

        // Panggil function didalam trait EggProductionChart
        $chartEggProductionPercentage = $this->chartEggProductionPercentage($rangeFilter, $periodFilter);

        $response = [
            "chart_growth" => [
                "chart_egg_production_percentage" => $chartEggProductionPercentage,
            ]

        ];
        return $response;
    }
}
