<?php

namespace App\Actions\Api;

use App\Http\Requests\Api\Auth\LoginRequest;
use App\Http\Requests\Api\Auth\RegisterRequest;
use App\Models\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthAction
{
    use DispatchesJobs;


    public function loginUser(LoginRequest $request)
    {
        $input = $request->only(["email", "password"]);

        if (!Auth::attempt($input)) {
            return "error";
        }

        $user = User::with('role')->where('email', $input['email'])->firstOrFail();

        if (!$user) {
            return "error";
        }

        if (Hash::check($input["password"], $user->password)) {
            $token = $user->createToken("SECRET TOKEN");
            return $token;
        } else {
            return "Password Error";
        }
    }


    public function registerUser(RegisterRequest $request)
    {
        $user = User::create([
            "name" => $request->name,
            "email" => $request->email,
            "username" => $request->username,
            "password" => bcrypt($request->password),
            "phone" => $request->phone
        ]);

        return $user;
    }
}
