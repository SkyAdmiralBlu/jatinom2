<?php

namespace App\Actions\Api;

use App\CustomTrait\Formatter;
use App\Http\Requests\Api\StoreEggProductionRequest;
use App\Http\Requests\Api\UpdateEggProductionRequest;
use App\Models\EggProduction;
use Illuminate\Foundation\Bus\DispatchesJobs;

class EggProductionAction
{
    use DispatchesJobs, Formatter;

    public function storeEggProduction(StoreEggProductionRequest $request)
    {
        $eggProduction = EggProduction::create([
            "weight" => $request->weight,
            "quantity" => $request->quantity,
            "record_date" => $request->record_date,
            "cage_id" => $request->cage_id,
        ]);

        return $eggProduction;
    }

    public function getAllEggProduction()
    {
        $eggProductions = EggProduction::paginate(10)->toArray();
        $data = $eggProductions['data'];
        $meta = $this->metaFormatter(
            $eggProductions['current_page'],
            $eggProductions['from'],
            $eggProductions['last_page'],
            $eggProductions['links'],
            $eggProductions['path'],
            $eggProductions['per_page'],
            $eggProductions['to'],
            $eggProductions['total']
        );

        $result = $this->paginationFormatter($meta, $data);
        return $result;
    }

    public function getEggProductionById(EggProduction $eggProduction)
    {
        return $eggProduction;
    }

    public function updateEggProduction(UpdateEggProductionRequest $request, EggProduction $eggProduction)
    {
        $input = $request->only([
            'weight',
            'quantity',
            'record_date',
            'cage_id'
        ]);
        $eggProduction->update($input);
        return $eggProduction;
    }

    public function deleteEggProductionById(EggProduction $eggProduction)
    {
        $eggProduction->delete();
        return $eggProduction;
    }
}
