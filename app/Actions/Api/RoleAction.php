<?php

namespace App\Actions\Api;

use App\Http\Requests\Api\StoreRoleRequest;
use App\Http\Requests\Api\UpdateRoleRequest;
use App\Models\Role;
use Illuminate\Foundation\Bus\DispatchesJobs;


class RoleAction
{
    use DispatchesJobs;

    public function storeRole(StoreRoleRequest $request)
    {
        $role = Role::create([
            "name" => $request->name,
        ]);

        return $role;
    }

    public function getAllRole()
    {
        $roles = Role::all();

        return $roles;
    }

    public function getRoleById(Role $role)
    {
        return $role;
    }

    public function updateRole(UpdateRoleRequest $request, Role $role)
    {
        $input = $request->only(['name']);
        $role->update($input);
        return $role;
    }

    public function deleteRoleById(Role $role)
    {
        $role->delete();
        return $role;
    }
}
