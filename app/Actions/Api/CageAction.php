<?php

namespace App\Actions\Api;

use App\CustomTrait\Formatter;
use App\Http\Requests\Api\StoreCageRequest;
use App\Http\Requests\Api\UpdateCageRequest;
use App\Models\Cage;
use Illuminate\Foundation\Bus\DispatchesJobs;


class CageAction
{
    use DispatchesJobs, Formatter;

    public function storeCage(StoreCageRequest $request)
    {
        $cage = Cage::create([
            "population" => $request->population,
            "status" => $request->status,
            "owner" => $request->owner,
        ]);

        return $cage;
    }

    public function getAllCage()
    {
        $cages = Cage::paginate(10)->toArray();
        $data = $cages['data'];
        $meta = $this->metaFormatter(
            $cages['current_page'],
            $cages['from'],
            $cages['last_page'],
            $cages['links'],
            $cages['path'],
            $cages['per_page'],
            $cages['to'],
            $cages['total']
        );

        $result = $this->paginationFormatter($meta, $data);
        return $result;
    }

    public function getCageById(Cage $cage)
    {
        return $cage;
    }

    public function updateCage(UpdateCageRequest $request, Cage $cage)
    {
        $input = $request->only([
            'population',
            'status',
            'owner'
        ]);
        $cage->update($input);
        return $cage;
    }

    public function deleteCage(Cage $cage)
    {
        $cage->delete();
        return $cage;
    }
}
