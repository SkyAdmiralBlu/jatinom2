<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cage extends Model
{
    use HasFactory;
    protected $guarded =[];

    public function egg_productions()
    {
        return $this->hasMany(EggProduction::class);
    }
}
