<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EggProduction extends Model
{
    use HasFactory;
    protected $guarded =[];

    public function cage()
    {
        return $this->belongsTo(Cage::class);
    }
}
