<?php

namespace App\CustomTrait;

trait Formatter
{
    private function metaFormatter($currentPage, $from, $lastPage, $links, $path, $perPage, $to, $total)
    {
        return [
            "current_page" => $currentPage,
            "from" => $from,
            "last_page" => $lastPage,
            "links" => $links,
            "path" => $path,
            "per_page" => $perPage,
            "to" => $to,
            "total" => $total
        ];
    }

    private function paginationFormatter($meta, $data)
    {
        return [
            "meta" => $meta,
            "data" => $data
        ];
    }
}
