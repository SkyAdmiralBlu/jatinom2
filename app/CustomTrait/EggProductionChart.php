<?php

namespace App\CustomTrait;

use App\Models\EggProduction;
use Carbon\Carbon;

trait EggProductionChart
{
    private function chartEggProductionPercentage($rangeFilter, $periodFilter)
    {
        // Menampilkan data dengan time frame 1 tahun
        if ($rangeFilter == 'year') {
            // jika query filter period ada
            if ($periodFilter) {
                $eggProductionData = EggProduction::with('cage')
                    ->whereYear('record_date', (int)$periodFilter)
                    ->get();
                $percentageArr = array();
                $dateArr = array();

                foreach ($eggProductionData as $value) {
                    array_push($percentageArr, $value->quantity / ($value->cage->population / 100));
                }


                foreach ($eggProductionData as  $value) {
                    array_push($dateArr, Carbon::parse($value->record_date)->format("Y-m-d"));
                }

                $data = [
                    "time_frame" => $dateArr,
                    "egg_production_percentage" => $percentageArr,
                ];

                return $data;
            }

            // jika query filter period tidak ada atau bernilai 0
            $eggProductionData = EggProduction::with('cage')
                ->whereYear('record_date', date('Y'))
                ->get();

            $percentageArr = array();
            $dateArr = array();

            foreach ($eggProductionData as $value) {
                array_push($percentageArr, $value->quantity / ($value->cage->population / 100));
            }

            foreach ($eggProductionData as  $value) {
                array_push($dateArr, Carbon::parse($value->record_date)->format("Y-m-d"));
            }

            $data = [
                "time_frame" => $dateArr,
                "egg_production_percentage" => $percentageArr,
            ];

            return $data;
        }

        // Menampilkan data dengan time frame 1 bulan
        if ($rangeFilter == 'month') {
            // jika query filter period ada
            if ($periodFilter) {
                $eggProductionData = EggProduction::with('cage')
                    ->whereYear('record_date', date('Y'))
                    ->whereMonth('record_date', (int)$periodFilter)
                    ->get();

                $percentageArr = array();
                $dateArr = array();

                foreach ($eggProductionData as $value) {
                    array_push($percentageArr, $value->quantity / ($value->cage->population / 100));
                }

                foreach ($eggProductionData as  $value) {
                    array_push($dateArr, Carbon::parse($value->record_date)->format("Y-m-d"));
                }

                $data = [
                    "time_frame" => $dateArr,
                    "egg_production_percentage" => $percentageArr,
                ];

                return $data;
            }

            // jika query filter period tidak ada atau bernilai 0
            $eggProductionData = EggProduction::with('cage')
                ->whereMonth('record_date', date('m'))
                ->get();

            $percentageArr = array();
            $dateArr = array();

            foreach ($eggProductionData as $value) {
                array_push($percentageArr, $value->quantity / ($value->cage->population / 100));
            }

            foreach ($eggProductionData as  $value) {
                array_push($dateArr, Carbon::parse($value->record_date)->format("Y-m-d"));
            }

            $data = [
                "time_frame" => $dateArr,
                "egg_production_percentage" => $percentageArr,
            ];

            return $data;
        }
    }
}
