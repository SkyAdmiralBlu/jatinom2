<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function sendResponse($code, $error = false, $message, $data, $meta = null)
    {
        if ($meta) {
            return response()->json([
                "code" => $code,
                "error" => $error,
                "message" => $message,
                "meta" => $meta,
                "data" => $data
            ]);
        }
        return response()->json([
            "code" => $code,
            "error" => $error,
            "message" => $message,
            "data" => $data
        ]);
    }

    public function sendError($code = 500, $error = true, $message, $data = null)
    {
        return response()->json([
            "code" => $code,
            "error" => $error,
            "message" => $message,
            "data" => $data
        ]);
    }
}
