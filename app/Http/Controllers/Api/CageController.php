<?php

namespace App\Http\Controllers\Api;

use App\Actions\Api\CageAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\StoreCageRequest;
use App\Http\Requests\Api\UpdateCageRequest;
use App\Models\Cage;

class CageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(CageAction $cageAction)
    {
        $cages = $cageAction->getAllCage();

        if ($cages) {
            return $this->sendResponse(200, false, "Berhasil mengambil data", $cages['data'], $cages['meta']);
        }

        return $this->sendError(400, true, "Gagal mengambil data");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Api\StoreCageRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCageRequest $request, CageAction $cageAction)
    {
        $cage = $cageAction->storeCage($request);

        if ($cage) {
            return $this->sendResponse(200, false, "Berhasil membuat data", $cage);
        }

        return $this->sendError(400, true, "Gagal membuat data");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cage  $cage
     * @return \Illuminate\Http\Response
     */
    public function show(Cage $cage)
    {
        if ($cage) {
            return $this->sendResponse(200, false, "Berhasil mengambil data", $cage);
        }

        return $this->sendError(400, true, "Gagal mengambil data");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Api\UpdateCageRequest  $request
     * @param  \App\Models\Cage  $cage
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCageRequest $request, Cage $cage, CageAction $cageAction)
    {
        $updateCage = $cageAction->updateCage($request, $cage);

        if ($updateCage) {
            return $this->sendResponse(200, true, "Berhasil mengubah data", $updateCage);
        }

        return $this->sendError(400, true, "Gagal mengubah data");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cage  $cage
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cage $cage, CageAction $cageAction)
    {
        $deleteCage = $cageAction->deleteCage($cage);

        if ($deleteCage) {
            return $this->sendResponse(200, false, "Berhasil menghapus data", null);
        }

        return $this->sendError(400, true, "Gagal menghapus data", null);
    }
}
