<?php

namespace App\Http\Controllers\Api;

use App\Actions\Api\UserAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\StoreUserRequest;
use App\Http\Requests\Api\UpdateUserRequest;
use App\Models\User;


class UserController extends Controller
{
    public function index(UserAction $userAction)
    {
        $user = $userAction->getAllUser();

        if ($user) {
            return $this->sendResponse(200, false, "Berhasil mengambil data", $user['data'], $user['meta']);
        }

        return $this->sendError(400, true, "Gagal mengambil data");
    }


    public function store(UserAction $userAction, StoreUserRequest $request)
    {
        $user = $userAction->createUser($request);

        if ($user) {
            return $this->sendResponse(200, false, "Berhasil membuat data", $user);
        }

        return $this->sendError(400, true, "Gagal membuat data");
    }

    public function show(UserAction $userAction, User $user)
    {
        $userByID = $userAction->getUserById($user);

        if ($userByID) {
            return $this->sendResponse(200, false, "Berhasil memanggil data", $userByID);
        }

        return $this->sendError(400, true, "Gagal memanggil data");
    }

    public function update(UserAction $userAction, User $user, UpdateUserRequest $updateUserRequest)
    {
        $user = $userAction->updateUserById($user, $updateUserRequest);

        if ($user) {
            return $this->sendResponse(200, false, "Berhasil mengubah data", $user);
        }

        return $this->sendError(400, true, "Gagal mengubah data");
    }


    public function destroy(UserAction $userAction, User $user)
    {
        $delete = $userAction->deleteUserById($user);

        if ($delete) {
            return $this->sendResponse(200, false, "Berhasil menghapus data", $delete);
        }

        return $this->sendError(400, true, "Gagal menghapus data");
    }
}
