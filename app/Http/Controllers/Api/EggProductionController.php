<?php

namespace App\Http\Controllers\Api;

use App\Actions\Api\EggProductionAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\StoreEggProductionRequest;
use App\Http\Requests\Api\UpdateEggProductionRequest;
use App\Models\EggProduction;

class EggProductionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(EggProductionAction $eggProductionAction)
    {
        $eggProduction = $eggProductionAction->getAllEggProduction();

        if ($eggProduction) {
            return $this->sendResponse(
                200,
                false,
                "Berhasil mengambil data",
                $eggProduction['data'],
                $eggProduction['meta']
            );
        }

        return $this->sendError(400, true, "Gagal mengambil data");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Api\StoreEggProductionRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEggProductionRequest $request, EggProductionAction $eggProductionAction)
    {
        $eggProduction = $eggProductionAction->storeEggProduction($request);

        if ($eggProduction) {
            return $this->sendResponse(200, false, "Berhasil membuat data", $eggProduction);
        }

        return $this->sendError(400, true, "Gagal membuat data");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EggProduction  $eggProduction
     * @return \Illuminate\Http\Response
     */
    public function show(EggProduction $eggProduction, EggProductionAction $eggProductionAction)
    {
        $eggProduction = $eggProductionAction->getEggProductionById($eggProduction);

        if ($eggProduction) {
            return $this->sendResponse(200, false, "Berhasil mengambil data", $eggProduction);
        }

        return $this->sendError(400, true, "Gagal mengambil data");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Api\UpdateEggProductionRequest  $request
     * @param  \App\Models\EggProduction  $eggProduction
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEggProductionRequest $request, EggProduction $eggProduction, EggProductionAction $eggProductionAction)
    {
        $updateEggProduction = $eggProductionAction->updateEggProduction($request, $eggProduction);

        if ($updateEggProduction) {
            return $this->sendResponse(200, false, "Berhasil mengubah data", $updateEggProduction);
        }

        return $this->sendError(400, true, "Gagal mengubah data");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EggProduction  $eggProduction
     * @return \Illuminate\Http\Response
     */
    public function destroy(EggProduction $eggProduction, EggProductionAction $eggProductionAction)
    {
        $deleteEggProduction = $eggProductionAction->deleteEggProductionById($eggProduction);

        if ($deleteEggProduction) {
            return $this->sendResponse(200, false, "Berhasil menghapus data", null);
        }

        return $this->sendError(400, true, "Gagal menghapus data", null);
    }
}
