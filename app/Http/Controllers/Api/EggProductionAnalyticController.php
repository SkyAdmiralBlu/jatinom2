<?php

namespace App\Http\Controllers\Api;

use App\Actions\Api\EggProductionAnalyticAction;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EggProductionAnalyticController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, EggProductionAnalyticAction $eggProductionAnalyticAction)
    {
        $response = $eggProductionAnalyticAction->getEggProductionAnalytic($request);
        if ($response) {
            return $this->sendResponse(200, false, "Berhasil mengambil data", $response);
        }
        return $this->sendError(400, false, "Gagal mengambil data", null);
    }
}
