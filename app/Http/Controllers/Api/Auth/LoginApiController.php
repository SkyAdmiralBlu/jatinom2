<?php

namespace App\Http\Controllers\Api\Auth;

use App\Actions\Api\AuthAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Auth\LoginRequest;
use Illuminate\Http\Request;

class LoginApiController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  LoginRequest $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(LoginRequest $request, AuthAction $authAction)
    {
        $response = $authAction->loginUser($request);

        if ($response == "error") {
            return $this->sendError(401, true, "Login Gagal");
        } else if ($response == "Password Error") {
            return $this->sendError(402, true, "Password salah!");
        } else {
            return $this->sendResponse(200, false, "Login Berhasil", [
                "token" => $response->plainTextToken,
                "token_type" => "Bearer",
                "expires_in" => 1400
            ]);
        }
    }
}
