<?php

namespace App\Http\Controllers\Api\Auth;

use App\Actions\Api\AuthAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Auth\RegisterRequest;

class RegisterApiController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \App\Http\Requests\Api\Auth\RegisterRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(RegisterRequest $request, AuthAction $authAction)
    {

        $user = $authAction->registerUser($request);

        // Success Create User
        if ($user) {
            return $this->sendResponse(200, false, "Register Berhasil", $user);
        }

        // Failed Create User
        return $this->sendError(402, true, "Register Gagal");
    }
}
