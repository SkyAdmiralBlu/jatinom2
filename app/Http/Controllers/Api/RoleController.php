<?php

namespace App\Http\Controllers\Api;

use App\Actions\Api\RoleAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\StoreRoleRequest;
use App\Http\Requests\Api\UpdateRoleRequest;
use App\Models\Role;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(RoleAction $roleAction)
    {
        $roles = $roleAction->getAllRole();

        if ($roles) {
            return $this->sendResponse(200, false, "Berhasil mengambil data", $roles);
        }

        return $this->sendError(500, true, "Gagal mengambil data");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Api\StoreRoleRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRoleRequest $request, RoleAction $roleAction)
    {
        $role = $roleAction->storeRole($request);

        if ($role) {
            return $this->sendResponse(200, false, "Berhasil membuat data", $role);
        }

        return $this->sendError(402, true, "Gagal membuat data");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role, RoleAction $roleAction)
    {
        $role = $roleAction->getRoleById($role);

        if (!$role) {
            return $this->sendError(404, true, "Data tidak ditemukan");
        }

        if ($role) {
            return $this->sendResponse(200, false, "Berhasil mengambil data", $role);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Api\UpdateRoleRequest  $request
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRoleRequest $request, Role $role, RoleAction $roleAction)
    {
        $updateRole = $roleAction->updateRole($request, $role);

        if ($updateRole) {
            return $this->sendResponse(200, false, "Berhasil mengubah data", $updateRole);
        }
        return $this->sendError(422, true, "Gagal mengubah data");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role, RoleAction $roleAction)
    {
        $deleteRole = $roleAction->deleteRoleById($role);

        if ($deleteRole) {
            return $this->sendResponse(200, false, "Berhasil menghapus data", null);
        }
        return $this->sendError(402, true, "Gagal menghapus data", null);
    }
}
