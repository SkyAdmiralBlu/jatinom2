<?php

namespace Database\Seeders;

use App\Models\Cage;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Cage::create([
            'population' => 5000,
            "status" => false,
            "owner" => "Jatinom Indah Agri"
        ]);
        Cage::create([
            'population' => 6000,
            "status" => false,
            "owner" => "Jatinom Indah Agri"
        ]);
        Cage::create([
            'population' => 3000,
            "status" => true,
            "owner" => "Jatinom Indah Agri"
        ]);
    }
}
