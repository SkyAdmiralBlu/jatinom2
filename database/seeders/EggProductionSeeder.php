<?php

namespace Database\Seeders;

use App\Models\EggProduction;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class EggProductionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for ($i = 1; $i <= 12; $i++) {
            for ($j = 1; $j <= 31; $j++) {
                EggProduction::create([
                    'weight' => 2000 + ($j * 10),
                    "quantity" => 20 + ($j * 10),
                    "record_date" => "2020-{$i}-{$j} 09:51:21",
                    "cage_id" => 1
                ]);
            }
        }

        for ($i = 1; $i <= 12; $i++) {
            for ($j = 1; $j <= 31; $j++) {
                EggProduction::create([
                    'weight' => 2000 + ($j * 10),
                    "quantity" => 20 + ($j * 10),
                    "record_date" => "2021-{$i}-{$j} 09:51:21",
                    "cage_id" => 1
                ]);
            }
        }

        for ($i = 1; $i <= 12; $i++) {
            for ($j = 1; $j <= 31; $j++) {
                EggProduction::create([
                    'weight' => 2000 + ($j * 10),
                    "quantity" => 20 + ($j * 10),
                    "record_date" => "2022-{$i}-{$j} 09:51:21",
                    "cage_id" => 1
                ]);
            }
        }
    }
}
