<?php

use App\Http\Controllers\Api\Auth\LoginApiController;
use App\Http\Controllers\Api\Auth\RegisterApiController;
use App\Http\Controllers\Api\RoleController;
use App\Http\Controllers\Api\Auth\UserDetailController;
use App\Http\Controllers\Api\EggProductionController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\CageController;
use App\Http\Controllers\Api\EggProductionAnalyticController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function () {
    // WRITE YOUR ENDPOINT INSIDE 
    Route::prefix("auth")->group(function () {
        Route::post("register", RegisterApiController::class);
        Route::post("login", LoginApiController::class);
        Route::middleware("auth:sanctum")->post("me", UserDetailController::class);
    });

    Route::middleware('auth:sanctum')->group(function () {
        // WRITE YOUR PROTECTED ROUTE
        Route::resource("role", RoleController::class);
        Route::resource("egg-production", EggProductionController::class);
        Route::get('egg-production-analytic', EggProductionAnalyticController::class);
        Route::resource("cage", CageController::class);
        Route::resource("user", UserController::class);
    });
});
